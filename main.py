from flask import Flask, render_template
import pwd
import os
import re
import glob
import time
import json
import urllib2
from Tkinter import *
import multiprocessing as mp
import htmlPy

PROC_TCP = "/proc/net/tcp"
PROC_UDP = "/proc/net/udp"
STATE = {
        '01':'ESTABLISHED',
        '02':'SYN_SENT',
        '03':'SYN_RECV',
        '04':'FIN_WAIT1',
        '05':'FIN_WAIT2',
        '06':'TIME_WAIT',
        '07':'CLOSE',
        '08':'CLOSE_WAIT',
        '09':'LAST_ACK',
        '0A':'LISTEN',
        '0B':'CLOSING'
        }

def _udp_load():
    ''' Read the table of tcp connections & remove header  '''
    with open(PROC_UDP,'r') as f:
        content = f.readlines()
        content.pop(0)
    return content

def _tcp_load():
    ''' Read the table of tcp connections & remove header  '''
    with open(PROC_TCP,'r') as f:
        content = f.readlines()
        content.pop(0)
    return content

def _hex2dec(s):
    return str(int(s,16))

def _ip(s):
    ip = [(_hex2dec(s[6:8])),(_hex2dec(s[4:6])),(_hex2dec(s[2:4])),(_hex2dec(s[0:2]))]
    return '.'.join(ip)

def _remove_empty(array):
    return [x for x in array if x !='']

def _convert_ip_port(array):
    host,port = array.split(':')
    return _ip(host),_hex2dec(port)

def udp_netstat():
    '''
    Function to return a list with status of tcp connections at linux systems
    To get pid of all network process running on system, you must run this script
    as superuser
    '''

    content=_udp_load()
    result = []
    for line in content:
        line_array = _remove_empty(line.split(' '))     # Split lines and remove empty spaces.
        l_host,l_port = _convert_ip_port(line_array[1]) # Convert ipaddress and port from hex to decimal.
        r_host,r_port = _convert_ip_port(line_array[2]) 
        tcp_id = line_array[0]
        state = STATE[line_array[3]]
        uid = pwd.getpwuid(int(line_array[7]))[0]       # Get user from UID.
        inode = line_array[9]                           # Need the inode to get process pid.
        pid = _get_pid_of_inode(inode)                  # Get pid prom inode.
        try:                                            # try read the process name.
            exe = os.readlink('/proc/'+pid+'/exe')
        except:
            exe = None

        nline = [tcp_id, uid, l_host+':'+l_port, r_host, state, pid, exe]
        result.append(nline)
    return result

def tcp_netstat():
    '''
    Function to return a list with status of tcp connections at linux systems
    To get pid of all network process running on system, you must run this script
    as superuser
    '''

    content=_tcp_load()
    result = []
    for line in content:
        line_array = _remove_empty(line.split(' '))     # Split lines and remove empty spaces.
        l_host,l_port = _convert_ip_port(line_array[1]) # Convert ipaddress and port from hex to decimal.
        r_host,r_port = _convert_ip_port(line_array[2]) 
        tcp_id = line_array[0]
        state = STATE[line_array[3]]
        uid = pwd.getpwuid(int(line_array[7]))[0]       # Get user from UID.
        inode = line_array[9]                           # Need the inode to get process pid.
        pid = _get_pid_of_inode(inode)                  # Get pid prom inode.
        try:                                            # try read the process name.
            exe = os.readlink('/proc/'+pid+'/exe')
        except:
            exe = None

        nline = [tcp_id, uid, l_host+':'+l_port, r_host, state, pid, exe]
        result.append(nline)
    return result

def _get_pid_of_inode(inode):
    '''
    To retrieve the process pid, check every running process and look for one using
    the given inode.
    '''
    for item in glob.glob('/proc/[0-9]*/fd/[0-9]*'):
        try:
            if re.search(inode,os.readlink(item)):
                return item.split('/')[2]
        except:
            pass
    return None

def begins_with(str1,str2):
    return str1[0:len(str2)]==str2


monitored_ips_prefix = "192.168.10.1"
monitored_USRPS=[{"name":"KIT4","ip":"192.168.10.104","used_by":None},
                     {"name":"KIT6","ip":"192.168.10.106","used_by":None}, 
                     {"name":"KIT7","ip":"192.168.10.107","used_by":None},
                     {"name":"KIT8","ip":"192.168.10.106","used_by":None}]

names = [i["name"] for i in monitored_USRPS]



app = Flask(__name__)
app.template_folder='.'

@app.route('/')
def monitor_testbed():
	return render_template('index.html', 
		lines=[0, 1, 2, 3], 
		cols=[1,2,3,4,5], 
		used_by=[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1])

@app.route('/update')
def update_USRPs():
	vars = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,-1, -1, -1, -1, -1, -1, -1, -1, -1]
	for i in range(len(vars)):
		ip = 101+i
		r = os.system("fping -q -c 1 -t 50 0.0 192.168.10." + str(ip))
		if r==0:
			vars[i] = 0
		else:
			vars[i] = -1
	for conn in udp_netstat():
		if begins_with(conn[3], monitored_ips_prefix):
			#print conn[1], conn[3]
			vars[int(conn[3].replace(".",""))-19216810101] = conn[1]
	for conn in tcp_netstat():
		if begins_with(conn[3], monitored_ips_prefix) and conn[4]=='ESTABLISHED':
			vars[int(conn[3].replace(".",""))-19216810101] = conn[1]
	
	return render_template('container.html', 
		lines=[0, 1, 2, 3], 
		cols=[1,2,3,4,5], 
		used_by=vars)
