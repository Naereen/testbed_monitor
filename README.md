# SCEE Testbed Monitor #

This repository contains the code of the SCEE USRP testbed monitor.
The SCEE USRP testbed is a software defined radio (SDR) testbed composed. of various Ettus USRP cards (N210, E110, B210)
![IMG_20170222_153048.jpg](https://bitbucket.org/repo/4x44qM/images/1718955135-IMG_20170222_153048.jpg)

This monitoring tool gives a live view of the usage of each USRP card by members of the team. Here for example, two different members are each using a specific board.
![screenshot.png](https://bitbucket.org/repo/4x44qM/images/151695219-screenshot.png)

### Dependencies ###
* Flask
* The code is though to answer a specific testbed setup, so we could say it is still dependent on the setup you have. This will hopefully change someday

### How to set it up ###
```shell
$ cd testbed_monitor
$ export FLASK_APP=main.py
$ python -m flask run --host=0.0.0.0 --port=8000
```

→ The monitoring tool is then accessible at the IP address of the machine that runs it, on port 8000.

### ToDo ###

* Document code
* Make code easier to parameterize, remove all hardcoded parameters
* Change remaining small defects in the GUI (border alignment)
* ...

---

### Who do I talk to? ###

* Quentin Bodinier ([@Kant_Breizh](https://bitbucket.org/Kant_Breizh)), Ph.D. Student [@scee_team](https://bitbucket.org/scee_ietr/)

## :scroll: License ?
[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://mit-license.org/)
[![Open Source Love svg3](https://badges.frapsoft.com/os/v3/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://bitbucket.org/scee_ietr/)
[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://bitbucket.org/scee_ietr/testbed_monitor/)